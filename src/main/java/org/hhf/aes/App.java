package org.hhf.aes;

import org.hhf.aes.core.DecryptUtil;
import org.hhf.aes.core.EncryptUtil;

import java.util.Base64;

/**
 * @description: AES加解密简单测试.
 * @author: hhf
 * @create: 2021-03-31 15:46
 **/
public class App {

    public static void main(String[] args) {
        String jsonStr = "{\"a\":\"1\",\"b\":\"2\"}";
        String encryptStr = Base64.getEncoder().encodeToString(EncryptUtil.encrypt(jsonStr));
        System.out.println(encryptStr);
        String decipherStr = DecryptUtil.decipher(encryptStr);
        System.out.println(decipherStr);
    }
}
