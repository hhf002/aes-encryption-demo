package org.hhf.aes.core;

import java.util.List;

public class ByteUtil{

	public static byte[] sysCopy(List<byte[]> srcArrays) {
        if (srcArrays != null && !srcArrays.isEmpty()) {
            int len = 0;
            for (byte[] srcArray : srcArrays) {
                if (srcArray != null) {
                    len += srcArray.length;
                }
            }
            byte[] destArray = new byte[len];
            int destLen = 0;
            for (byte[] srcArray : srcArrays) {
                if (srcArray != null) {
                    System.arraycopy(srcArray, 0, destArray, destLen, srcArray.length);
                    destLen += srcArray.length;
                }
            }
            return destArray;
        }
        return new byte[0];
    }
}