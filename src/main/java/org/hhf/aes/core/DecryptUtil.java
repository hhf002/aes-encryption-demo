package org.hhf.aes.core;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class DecryptUtil {

    public static final byte[] decipher(byte[] requestBody) {
        byte[] infoB = AESTool.decrypt(requestBody, KeysUtil.key());
        return JSONDateZip.unzip(infoB);
    }

    public static String decipher(String info) {
        byte[] deinfo = Base64.getDecoder().decode(info);
        byte[] infoB = AESTool.decrypt(deinfo, KeysUtil.key());
        String deComPressInfo = null;
        try {
            deComPressInfo = new String(JSONDateZip.unzip(infoB), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return deComPressInfo;
    }

    public static void main(String[] args) {
        String decipher = decipher("HrwZitNlAVyd8dZiyLvLcRrRZWUMMspfkW6e4xiCZxY=");
        System.out.println(decipher);
    }
}