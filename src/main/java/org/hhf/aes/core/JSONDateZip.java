package org.hhf.aes.core;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class JSONDateZip {

    public static byte[] zip(byte[] input) {
        ByteArrayOutputStream output = null;
        try {
            Deflater deflater = new Deflater();
            deflater.setInput(input);
            deflater.finish();
            output = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            while (!deflater.finished()) {
                int byteCount = deflater.deflate(buf);
                output.write(buf, 0, byteCount);
            }
            deflater.end();
            return output.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (output != null) {
                    output.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return input;
    }

    public static byte[] unzip(byte[] buf) {
        ByteArrayOutputStream output = null;
        try {
            Inflater inflater = new Inflater();
            inflater.setInput(buf, 0, buf.length);
            output = new ByteArrayOutputStream();
            byte[] result = new byte[1024];
            int size = -1;
            while (size != 0) {
                size = inflater.inflate(result, 0, result.length);
                output.write(result, 0, size);
            }
            inflater.end();
            result = output.toByteArray();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (output != null) {
                    output.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return buf;
    }
}