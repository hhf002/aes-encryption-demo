package org.hhf.aes.core;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class EncryptUtil{

	public static final byte[] encrypt(byte[] responseBody){
		byte[] zip = JSONDateZip.zip(responseBody);
		return AESTool.encrypt(zip, KeysUtil.key());
	}
	
	public static byte[] encrypt(String jsonStr) {
		byte[] infoB = new byte[0];
		try {
			infoB = JSONDateZip.zip(jsonStr.getBytes("UTF-8"));
			infoB = AESTool.encrypt(infoB, KeysUtil.key());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return infoB;
	}

	public static void main(String[] args) {
		String jsonStr = "{\"a\":\"1\",\"b\":\"2\"}";
		String s = Base64.getEncoder().encodeToString(encrypt(jsonStr));
		System.out.println(s);
	}
}