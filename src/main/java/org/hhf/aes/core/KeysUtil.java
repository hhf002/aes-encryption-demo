package org.hhf.aes.core;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class KeysUtil {

    public static final String key = "123456789";
    public static final String key1 = "987654321";

    public static byte[] key() {
        byte[] kByte1 = MD5Tool.getMD5Str(KeysUtil.key1);
        byte[] kByte2 = MD5Tool.getMD5Str(KeysUtil.key);
        List<byte[]> byteList = new ArrayList<byte[]>();
        byteList.add(kByte1);
        byteList.add(kByte2);
        return ByteUtil.sysCopy(byteList);
    }

    public static void main(String[] args) throws IOException {
        File file = new File("D:\\sec.zip");
        FileOutputStream fos = new FileOutputStream(file);
        InputStream is = new ByteArrayInputStream(key());
        int len = 0;
        byte[] buff = new byte[1024];
        while ((len = is.read(buff)) != -1) {
            fos.write(buff, 0, len);
        }
        is.close();
        fos.close();
    }
}